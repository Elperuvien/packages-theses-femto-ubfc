# Package de rédaction

Ces fichiers sont fournis afin de faciliter et d'homogénéiser les thèse au sein
du DMA voire de l'université.  Ainsi il propose une mise en page "clé en main"
d'une thèse de doctorat

En cas de problème, ouvrez un issue dans le dépot git 

Un template est associé aux packages pour la compilation de la thèse disponible
dans [ce dépot](https://gitlab.com/Elperuvien/femto-st-template-these)

## Installation

### Linux

- Cloner le dépot (ou a défaut copier les fichiers) dans l'arbre texmf de votre
  distribution latex

### Windows

- Distribution [TEXLIVE](https://www.tug.org/texlive/windows.html)
	- Cloner de dépot ( ou a défaut copier les fichiers) dans :
	`chemin/vers/utilisateur/texmf/tex/latex/local/`
	Si les dossiers n'exitent pas, créer les !

- Distribution Miktex
	- _je ne sais pas !!! Donner la méthode d'installation si vous l'avez faite !_

## Mise en garde

- Il est recommandé d'utiliser latexmk ou un autre utilitaire pour une
  compilation automatique de la thèse.

- Le template est distribué sans aucune garantie.

- **ATTENTION : si vous cloner le dépot, il ne faut pas commiter votre thèse,
  il est préférable de copier ce template dans votre répertoire**

## Liste des packages utilisés et leur fonction

 - babel : redaction en français par défaut
 - inputenc : utf8 [Tous les fichiers tex doivent être encodé en utf-8]

### Gestion des titres des flottants

 - caption
 - subcaption

### Gestion des flottants

 - placeins : bloque les flottants à la section
 - nowidow : evite les veuves et les orphelines

### Gestion des tableaux

 - booktabs
 - multirow
 - multicol

### Gestion des images

 - graphicx

### Ecriture des maths

 - amsmath
 - amssymb
 - cancel : permet de barrer du texte

### Gestion des couleurs

 - xcolor

### Mise en forme des titres

 - titlesec
 - titletoc

### Bibliographie et renvois

 - csquotes
 - biblatex
 - cleveref

### Marges

 - geometry
 - fancyhdr

### Unités SI

 - siunitx

### Package spécifiquement écrit pour la mise en page 

 - UBFC_edSpim_titre
 - quatrieme_couverture_spim
