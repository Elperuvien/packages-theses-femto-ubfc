\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{these}[2019/07/27 Classe pour la these]
\renewcommand{\normalsize}{\fontsize{10pt}{12pt}\selectfont}
\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8in}
\pagenumbering{arabic}

\LoadClass[a4paper, dvipsnames, twoside, 12pt]{book}

\RequirePackage{ae, lmodern}
\RequirePackage[french]{babel}
\frenchbsetup{StandardLists=true}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

\RequirePackage[
	justification=centering]{caption}
\RequirePackage{subcaption}

\DeclareCaptionFont{bleuFemto}{\color{bleuFemto}}
\DeclareCaptionFont{gris}{\color{grisFemto}}

\captionsetup{labelfont={bleuFemto},textfont=bleuFemto}

\RequirePackage{booktabs}
\RequirePackage{multirow}
\RequirePackage{multicol}

\RequirePackage{graphicx}

% Evite aux flottants de changer de section
\RequirePackage[section]{placeins}

\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{cancel}

% Définition des couleurs

\RequirePackage{xcolor}

\definecolor{bleuFemto}{RGB}{39,84,148}
\definecolor{vertFemto}{RGB}{175,203,81}

\RequirePackage{biblio_spim}

\RequirePackage[indent]{parskip}
\RequirePackage{formats_titres}
\RequirePackage[titletoc]{appendix}

% Réglage du package hyperref

\RequirePackage{hyperref} %Bien charger hyperref après formats_titres !

\hypersetup{
	colorlinks=true,
		breaklinks=true,
		urlcolor=blue,
		linkcolor = bleuFemto,
		citecolor = bleuFemto,
}

\addto\extrasfrench{%
	\def\sectionautorefname{section}
	\def\subsectionautorefname{section}
	\def\subsubsectionautorefname{section}
	\def\appendixautorefname{annexe}
}

\RequirePackage[
	a4paper,
	twoside,
	headheight=42pt,
	textheight=650pt,
	headsep=30pt,
	footskip=42pt,
	voffset=12pt,
]{geometry}%

\RequirePackage{entetes_pied_spim}

% Evitement des veuves et des orphelines
\clubpenalty=9999

\RequirePackage[%
    all,
    defaultlines=3 % nombre minimum de lignes
]{nowidow}

% Package pour les unités SI
\RequirePackage{siunitx}
\sisetup{locale = FR,
	 range-phrase = {~et~}}

% Package pour des références "clever" 
% \cref{}, \Cref{}
\RequirePackage[
	noabbrev, 
	nameinlink, 
	french]{cleveref}

\crefname{table}{tableau}{tableaux}
\Crefname{table}{Tableau}{Tableaux}
\crefname{equation}{équation}{équations}
\Crefname{equation}{Équation}{Équations}

% Ajout des longtable
\RequirePackage{longtable}

% Ajout des pages en paysage
\RequirePackage{lscape}

% Gestion des listes
\RequirePackage{enumitem}

% Definition de la page de garde

\RequirePackage{UBFC_edSpim_titre}
\RequirePackage{quatrieme_couverture_spim}
